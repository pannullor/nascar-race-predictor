"""
WSGI config for securedatacenter project.
It exposes the WSGI callable as a module-level variable named ``application``.
For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

project_home = os.path.dirname(os.path.abspath(__file__))
os.environ["DJANGO_SETTINGS_MODULE"] = "nascar_predictor.settings.settings"
application = get_wsgi_application()
